package com.samart.spyrecorder.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.samart.spyrecorder.app.util.L;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class ActivityServiceMessenger implements ActivityServiceMessengerInterface {
    /**
     * messenger to send message to this activity from service
     */
    private final Messenger mIncomingMessenger;
    private final ServiceMessageListener serviceMessageListener;
    /**
     * whether we have called bind on the service
     */
    public boolean mIsServiceBound = false;
    /**
     * Messenger for communication with service,
     * it delivers messages from service
     */
    private Messenger mServiceMessenger;
    /**
     * class for interacting with the main interface of the service
     * //by Google dev site
     */
    private ServiceConnection mServiceConnection = new ServiceConnection() {
        /**
         * called when connection to service established.
         * @param name forget
         * @param service service to connect with, pass this to messenger
         */
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mServiceMessenger = new Messenger(service);
            L.d("service connected");
            try {
                Message msg = Message.obtain(null,
                        ServiceMessenger.MSG_REGISTER_CLIENT);
                msg.replyTo = mIncomingMessenger;
                mServiceMessenger.send(msg);
                mIsServiceBound = true;
                serviceMessageListener.onServiceBound();
            } catch (RemoteException e) {
                //service crashed. probably is dead.
                L.d("service crashed");
            }
        }

        /**
         * called when service unexpectedly disconnected
         * @param name no care
         */
        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceMessageListener.onServiceUnbound();
            mIsServiceBound = false;
            mServiceMessenger = null;
            L.d("service disconnected");
        }
    };

    private Intent intentService;

    public ActivityServiceMessenger(ServiceMessageListener serviceMessageListener) {
        this.serviceMessageListener = serviceMessageListener;
        this.mIncomingMessenger = new Messenger(new IncomingHandler(serviceMessageListener));
    }

    /**
     * establish a connection with the service.
     */
    @Override
    public void doBindService() {
        if (mIsServiceBound) return;
        if (null == intentService) return;
        serviceMessageListener.doBindService(
                intentService,
                mServiceConnection,
                Context.BIND_NOT_FOREGROUND);
    }

    /**
     * unbinds service from activity
     * don't stop service
     */
    @Override
    public void doUnbindService() {
        if (null == intentService) return;
        if (mIsServiceBound) {
            if (mServiceMessenger != null) {
                try {
                    Message msg = Message.obtain(null, ServiceMessenger.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mIncomingMessenger;
                    mServiceMessenger.send(msg);
                } catch (RemoteException e) {
                    //crashed service.
                    L.d("service is crashed");
                }
            }
            serviceMessageListener.doUnbindService(mServiceConnection);
            mIsServiceBound = false;
        }
    }

    public void requestServiceState() {
        if (!mIsServiceBound) {
            L.d("requestServiceState when !mIsServiceBound");
            return;
        }
        Message msg = Message.obtain(null, ServiceMessenger.MSG_GET_STATE);
        try {
            mServiceMessenger.send(msg);
        } catch (RemoteException e) {
            //service probably died
            L.d("service probably died ");
        }
    }

    @Override
    public void callSetServiceEnabled(boolean isServiceEnabled) {
        if (!mIsServiceBound) {
            L.d("call setServiceEnabled when !mIsServiceBound");
            return;
        }
        Message msg = Message.obtain(null, ServiceMessenger.MSG_SET_ENABLED);
        msg.arg1 = isServiceEnabled ? 1 : 0;
        try {
            mServiceMessenger.send(msg);
        } catch (RemoteException e) {
            //service probably died
            L.d("service probably died ");
        }
    }

    @Override
    public boolean isServiceBound() {
        return mIsServiceBound;
    }

    @Override
    public void callStopService() {
        if (!mIsServiceBound) {
            L.d("callStopService while service is not bound");
            return;
        }
        Message msg = Message.obtain(null, ServiceMessenger.MSG_SET_ENABLED);
        msg.arg1 = 0;
        try {
            mServiceMessenger.send(msg);
        } catch (RemoteException e) {
            //service probably died
            L.d("service probably died ");
        }
    }

    @Override
    public void onServiceStarted(Intent intentService) {
        this.intentService = intentService;
        if (!mIsServiceBound)
            doBindService();
    }

    public interface ServiceMessageListener {
        void onServiceEnabled(boolean isEnabled);

        void onServiceRecording(boolean isRecording);

        void onServiceStopped();

        void onServiceBound();

        void onServiceUnbound();

        void doUnbindService(ServiceConnection mServiceConnection);

        void doBindService(Intent intentService, ServiceConnection mServiceConnection, int bindNotForeground);

        void onServiceBroadcastState(State state);

    }

    /**
     * handler for incoming messages from service
     */
    public static class IncomingHandler extends Handler {
        private ServiceMessageListener serviceMessageListener;

        public IncomingHandler(ServiceMessageListener listener) {
            this.serviceMessageListener = listener;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ServiceMessenger.MSG_GET_STATE:
                    L.d("activity received message broadcast state " + msg.arg1);
                    serviceMessageListener.onServiceBroadcastState(State.values()[msg.arg1]);
                    break;
                default:
                    L.d("activity messenger received unhandled message: " + msg.arg1);
                    super.handleMessage(msg);
                    break;
            }
        }

    }

}
