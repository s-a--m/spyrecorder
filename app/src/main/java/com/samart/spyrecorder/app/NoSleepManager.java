package com.samart.spyrecorder.app;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.PowerManager;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class NoSleepManager {
    private final Context context;
    private final PowerManager.WakeLock wakeLock;
    private MediaPlayer mSilentPlayer;

    public NoSleepManager(Context context) {
        this.context = context;
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        this.wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "spy_rec_service");
    }

    public void beginNoSleep() {
        wakeLock.acquire();
        this.mSilentPlayer = MediaPlayer.create(context, R.raw.silent);
        mSilentPlayer.setVolume(0.0f, 0.0f);
        mSilentPlayer.setLooping(true);
        mSilentPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mSilentPlayer.start();
    }

    public void stopNoSleep() {
        if (null != this.mSilentPlayer) {
            mSilentPlayer.stop();
            mSilentPlayer.reset();
            mSilentPlayer.release();
            mSilentPlayer = null;
            wakeLock.release();
        }
    }
}
