package com.samart.spyrecorder.app;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.samart.spyrecorder.app.util.L;

public class MainActivity extends ActionBarActivity
        implements ServiceStarter.ServiceStarterInterface, ActivityServiceMessenger.ServiceMessageListener {

    private static final boolean IS_DEBUG = false;
    private TextView textStatus;
    private ImageView imageButton;
    private ActivityServiceMessengerInterface activityServiceMessenger;
    private boolean isServiceEnabled = false;
    private boolean isServiceStarted = false;
    private boolean isServiceBound = false;

    private State serviceState = State.NULL_ST;
    private boolean toBeginListen;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onServiceBroadcastState(State state) {
        if (serviceState == state) return;
        serviceState = state;
        L.d("activity received state " + state);
        switch (state) {
            case NULL_ST:
                break;
            case SERVICE_CREATED:
                activityServiceMessenger.doBindService();
                activityServiceMessenger.requestServiceState();
                if(this.toBeginListen){
                    isServiceEnabled = true;
                    activityServiceMessenger.callSetServiceEnabled(true);
                    toBeginListen = false;
                }

                break;
            case SERVICE_WAIT:
                onServiceEnabled(false);
                break;
            case SERVICE_DESTROYED:
                onServiceEnabled(false);
                onServiceStopped();
                break;
            case SERVICE_LISTENING:
                onServiceEnabled(true);
                break;
            case SERVICE_RECORDING:
                onServiceRecording(true);
                break;
            default:
                throw new RuntimeException("unknown state " + state.toString());
        }
    }

    @Override
    public void onServiceEnabled(boolean isEnabled) {
        if (IS_DEBUG)
            L.d("activity onserviceenabled " + isEnabled);
        isServiceEnabled = isEnabled;
        if (isEnabled) {
            imageButton.setImageResource(R.drawable.mic_blue);
            textStatus.setVisibility(View.VISIBLE);
        } else {
            imageButton.setImageResource(R.drawable.mic_grey);
            textStatus.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onServiceRecording(boolean isRecording) {
        if (IS_DEBUG)
            L.d("activity onservicerecording" + isRecording);
        if (isRecording) {
            imageButton.setImageResource(R.drawable.mic_red);
            textStatus.setVisibility(View.VISIBLE);
        } else {
            if (isServiceEnabled) {
                imageButton.setImageResource(R.drawable.mic_blue);
                textStatus.setVisibility(View.VISIBLE);
            } else {
                imageButton.setImageResource(R.drawable.mic_grey);
                textStatus.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onServiceStopped() {
        if (IS_DEBUG)
            L.d("activity onServiceStopped");
        isServiceStarted = false;
        isServiceBound = false;
    }

    @Override
    public void onServiceBound() {
        if (IS_DEBUG)
            L.d("activity onServiceBound");
        activityServiceMessenger.requestServiceState();
        isServiceBound = true;
    }

    @Override
    public void onServiceUnbound() {
        if (IS_DEBUG)
            L.d("activity onServiceUnbound");
        isServiceBound = false;
    }

    @Override
    public void doUnbindService(ServiceConnection mServiceConnection) {
        if (IS_DEBUG)
            L.d("activity doUnbindService");
        unbindService(mServiceConnection);
    }

    @Override
    public void doBindService(Intent intentService, ServiceConnection mServiceConnection, int bindNotForeground) {
        if (IS_DEBUG)
            L.d("activity doBindService");
        bindService(intentService, mServiceConnection, bindNotForeground);
    }


    @Override
    protected void onStart() {
        if (IS_DEBUG)
            L.d("activity onStart");
        super.onStart();
        if (isServiceStarted)
            activityServiceMessenger.doBindService();
    }

    @Override
    protected void onStop() {
        if (IS_DEBUG)
            L.d("activity onStop");
        super.onStop();
        if (isServiceStarted && isServiceBound)
            activityServiceMessenger.doUnbindService();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (IS_DEBUG)
            L.d("activity OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.textStatus = (TextView) findViewById(R.id.text_status);
        this.imageButton = (ImageView) findViewById(R.id.image_button);
        this.activityServiceMessenger = new ActivityServiceMessenger(this);
        final ServiceStarter serviceStarter = new ServiceStarter(this);
        serviceStarter.setServiceStarted(true);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isServiceEnabled = !isServiceEnabled;
                if (!activityServiceMessenger.isServiceBound()) {
                    serviceStarter.setServiceStarted(true);
                    MainActivity.this.toBeginListen = true;
                }
                activityServiceMessenger.callSetServiceEnabled(isServiceEnabled);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public ComponentName callStartService(Intent intent) {
        if (IS_DEBUG)
            L.d("activity callStartService");
        return startService(intent);
    }

    @Override
    public void callStopService(Intent intent) {

        if (IS_DEBUG)
            L.d("activity callStopService");
        activityServiceMessenger.callStopService();
    }

    @Override
    public void onServiceStarted(Intent intentService) {
        if (IS_DEBUG)
            L.d("activity onServiceStarted");
        activityServiceMessenger.onServiceStarted(intentService);
        isServiceStarted = true;
    }
}
