package com.samart.spyrecorder.app;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import com.samart.spyrecorder.app.util.L;

import java.io.IOException;

/**
* Created by sss on 16.06.14.
*/
public class MicRecorderMan {
    private MediaRecorder micRecorder;
    private int sampleRate;
    private int[] sampleRates = new int[]{3690, 4200, 7380, 8400, 11025, 16000, 22050, 44100};
    private boolean isPrepared = false;

    public void prepareRecorder(String filename) {
        this.micRecorder = new MediaRecorder();
        micRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        micRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        micRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        micRecorder.setAudioEncodingBitRate(256 * 1024);
        micRecorder.setAudioSamplingRate(getMaxValidSampleRate());

        micRecorder.setOutputFile(filename);
        isPrepared = true;

    }

    /**
     * this method guaranteed that sample rate is ok
     * from http://stackoverflow.com/questions/11549709/how-can-i-find-out-what-sampling-rates-are-supported-on-my-tablet?lq=1
     *
     * @param sampleRate sample rate
     * @return is valid
     */
    private boolean isValidSampleRate(int sampleRate) {

        AudioRecord audioRecordTest = null;
        try {
            int bufferSize = AudioRecord.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT);
            audioRecordTest = new AudioRecord(MediaRecorder.AudioSource.MIC,
                    sampleRate, AudioFormat.CHANNEL_IN_MONO,
                    AudioFormat.ENCODING_PCM_16BIT, bufferSize);
        } catch (IllegalArgumentException e) {
            return false;
        } finally {
            if (audioRecordTest != null)
                audioRecordTest.release();
        }
        return true;
    }

    private int getMaxValidSampleRate() {
        if (this.sampleRate == 0) {
            for (int rInd = sampleRates.length - 1; rInd >= 0; rInd--) {
                if (isValidSampleRate(sampleRates[rInd])) {
                    sampleRate = sampleRates[rInd];
                    break;
                }
            }

        }
        return sampleRate;
    }

    public void startRecording() throws IOException {
        if (!isPrepared) throw new IllegalStateException("cal prepare first");
        micRecorder.prepare();
        micRecorder.start();
    }

    public void stopRecording() {
        micRecorder.stop();
        micRecorder.reset();
        micRecorder.release();
        micRecorder = null;
        isPrepared = false;
    }

    public void abortPreparation() {
        if(null!=micRecorder){
           try{
              stopRecording();
           }catch (Exception e){
              L.e("exception on abort preparation ",e);
           }
        }
    }
}
