package com.samart.spyrecorder.app;

import android.os.Environment;

import com.samart.spyrecorder.app.util.FileUtils;
import com.samart.spyrecorder.app.util.L;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
* Created by sss on 16.06.14.
*/
public class FilesManager {

    public String getOutputFileName() {
        String date = new SimpleDateFormat("yy-MM-dd_HH_mm_ss").format(new Date());
        String fileName = "record_" + date + ".mp4";
        File dir;
        if (FileUtils.isSDCardMounted()) {
            dir = Environment.getExternalStorageDirectory();
        } else {
            dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            if (!dir.mkdirs()) L.d("could not create directory " + dir.getAbsolutePath());
        }
        File appDir = FileUtils.createDirectory(dir, "SpyRecords");
        return FileUtils.createFile(appDir, fileName).getAbsolutePath();
    }
}
