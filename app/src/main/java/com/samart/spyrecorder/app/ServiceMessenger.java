package com.samart.spyrecorder.app;

import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.samart.spyrecorder.app.util.L;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class ServiceMessenger {
    /**
     * command to service to start action
     */
    public static final int MSG_PATTERN_MATCHED = 5;
    /**
     * command to the service to register a client, receiving callbacks
     * from the service.
     */
    public static final int MSG_REGISTER_CLIENT = 1;
    /**
     * command to the service to unregister a client, or stop receiving callbacks
     * from the service.
     */
    public static final int MSG_UNREGISTER_CLIENT = 2;
    /**
     * command to service to set it enabled or disabled
     */
    public static final int MSG_SET_ENABLED = 4;
    /**
     * message from service that it will be destroyed
     */
    public static final int MSG_GET_STATE = 11;
    /**
     * target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger;

    public ServiceMessenger(MessengerListener serviceState) {
        this.mMessenger = new Messenger(new IncomingHandler(serviceState));
    }

    public IBinder getBinder() {
        return this.mMessenger.getBinder();
    }

    public void notifyStateRecording() {
        notifyState();
    }


    public void notifyState() {
        L.d("notify state recording ");
        try {
            this.mMessenger.send(Message.obtain(null, MSG_GET_STATE));
        } catch (RemoteException e) {
            L.e("could not send message ", e);
        }

    }

    public void notifyStateListening() {
        notifyState();
    }

    public void notifyServiceDestroyed() {
        notifyState();
    }

    public void onStopService() {
        notifyState();
    }

    private static class IncomingHandler extends Handler {

        private final MessengerListener serviceState;
        /**
         * keeps track of all current registered clients
         */
        private List<Messenger> mClients = new ArrayList<Messenger>();

        public IncomingHandler(MessengerListener serviceState) {
            this.serviceState = serviceState;

        }

        @Override
        public void handleMessage(Message msg) {
            L.d("service incoming handler handle message " + msg.what + " " + msg.arg1);
            switch (msg.what) {
                case MSG_GET_STATE:
                    L.d("service received MSG_REGISTER_CLIENT");
                    broadcastState();
                    break;
                case MSG_REGISTER_CLIENT:
                    L.d("service received MSG_REGISTER_CLIENT");
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    L.d("service received MSG_REMOVE_CLIENT");
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_SET_ENABLED:
                    L.d("service received MSG_SET_ENABLED");
                    this.setServiceEnabled(msg.arg1 > 0);
                    break;
                case MSG_PATTERN_MATCHED:
                    L.d("service received MSG_PATTERN_MATCHED");
                    this.toggleRecording();
                    break;
                default:
                    super.handleMessage(msg);
                    break;

            }
        }


        private void toggleRecording() {
            serviceState.onMessageToggleRecording();
        }


        private void broadcastState() {
            broadcastInt(serviceState.getState(), MSG_GET_STATE);
        }

        private void broadcastInt(int arg, int msgCode) {
            for (int i = mClients.size() - 1; i >= 0; i--) {
                try {
                    Message message = Message.obtain(null, msgCode);
                    message.arg1 = arg;
                    mClients.get(i).send(message);
                } catch (RemoteException e) {
                    //client is dead.
                    mClients.remove(i);
                }
            }
        }


        public void setServiceEnabled(boolean isSetToEnabled) {
            if (isSetToEnabled) serviceState.onMessageStartListening();
            else serviceState.onMessageStopService();
        }

    }
}
