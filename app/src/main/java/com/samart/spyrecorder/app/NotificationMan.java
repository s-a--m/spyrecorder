package com.samart.spyrecorder.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Vibrator;

import com.samart.spyrecorder.app.util.Toaster;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class NotificationMan implements ServiceNotificationListener {
    private static final long[] PATTERN_VIBRATE_START = new long[]{0L, 40L, 100L, 40L, 100L, 10L};
    private static final long[] PATTERN_VIBRATE_STOP = new long[]{0L, 40L, 100L, 200L};
    private final Context context;
    private final Notification.Builder builder;
    private final Vibrator vibrator;
    private final NotificationManager mNotificationManger;
    private boolean isVisualEnabled = true;
    private Notification mNotification;

    public NotificationMan(Context context) {
        this.isVisualEnabled = SettingsFragment.isNotifyEnabled(context);
        this.context = context;
        this.builder = new Notification.Builder(context);
        createNotification();
        this.vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        this.mNotificationManger = (NotificationManager) context.getSystemService(Service.NOTIFICATION_SERVICE);

    }

    @SuppressWarnings("Deprecated")
    /**
     * shows mNotification with this service started
     */
    private void showNotification(CharSequence title, CharSequence text, CharSequence subtext, int resId) {
        if (title != null) {
            builder.setContentTitle(title);
        }
        if (text != null) {
            builder.setContentText(text);
        }
        if (subtext != null) {
            if (Build.VERSION.SDK_INT >= 16)
                builder.setSubText(subtext);
        }
        if (resId > 0) {
            builder.setSmallIcon(resId);
        }
        this.mNotification = builder.getNotification();
        mNotificationManger.notify(R.string.service_waiting_msg, mNotification);
    }


    private void cancelNotification() {
        mNotificationManger.cancel(R.string.service_waiting_msg);
    }

    private void createNotification() {
        CharSequence text = context.getText(R.string.service_waiting_msg);
        Intent activityIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(
                context, 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentTitle("Spy recorder");
        builder.setContentText(text);
        builder.setSmallIcon(R.drawable.ic_stat_device_access_mic_muted);
        builder.setOngoing(true);
        builder.setContentIntent(contentIntent);
        this.mNotification = builder.getNotification();
    }

    @Override
    public void onFinishListening() {
        if (isVisualEnabled) {
            Toaster.notify(context, R.string.service_stopped);
            cancelNotification();
        }
    }

    @Override
    public void onStartRecording(String outputFileName) {
        this.vibrator.vibrate(PATTERN_VIBRATE_START, -1);
        if (isVisualEnabled)
            showNotification(null, "Recording", outputFileName, R.drawable.ic_stat_device_access_mic);
    }

    @Override
    public void onStopRecording() {
        this.vibrator.vibrate(PATTERN_VIBRATE_STOP, -1);
        if (isVisualEnabled)
            showNotification(null, "Waiting", "", R.drawable.ic_stat_device_access_mic_muted);
    }

    @Override
    public void onStopService() {
        onFinishListening();
    }

    @Override
    public void onStartListening() {
        if (isVisualEnabled)
            showNotification(null, "Waiting", "", R.drawable.ic_stat_device_access_mic_muted);
    }
}
