package com.samart.spyrecorder.app.util;

import android.util.Log;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public final class L {
    private static final String TAG = "spy_recorder";
    private static final boolean IS_DEBUG = false;

    public static void d(String m) {
        if (IS_DEBUG)
            Log.d(TAG, " " + Thread.currentThread().getName() + " " + m);
    }

    public static void e(String m, Throwable e) {
        Log.e(TAG, " " + Thread.currentThread().getName() + " " + m);
        e.printStackTrace();
    }
}
