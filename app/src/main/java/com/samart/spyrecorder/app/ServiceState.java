package com.samart.spyrecorder.app;

import com.samart.spyrecorder.app.util.L;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class ServiceState implements MessengerListener, ServiceStateListener {
    private final VolumeButtonListenerService.ServiceController serviceController;
    private State state = State.NULL_ST;

    public ServiceState(VolumeButtonListenerService.ServiceController serviceController) {
        this.serviceController = serviceController;
    }

    @Override
    public String toString() {
        return state.toString();
    }

    @Override
    public boolean isRecording() {
        return state == State.SERVICE_RECORDING;
    }

    @Override
    public int getState() {
        return state.ordinal();
    }

    @Override
    public boolean isListening() {
        return state == State.SERVICE_LISTENING;
    }

    @Override
    public void onServiceCreate() {
        L.d("service state on service created");
        state = State.SERVICE_CREATED;

    }

    @Override
    public void onServiceDestroy() {
        L.d("service state on service destroy");
        state = State.SERVICE_DESTROYED;
    }

    @Override
    public void onMessageStartListening() {
        L.d("service state onMessageStartListening");
        state = State.SERVICE_LISTENING;
        serviceController.startListening();
    }

    @Override
    public void onMessageStopListening() {
        L.d("service state onMessageStopListening");
        if (isRecording()) stopRecording();
        state = State.SERVICE_WAIT;
        serviceController.finishListening();
    }

    @Override
    public void onMessageToggleRecording() {
        L.d("service state onMessageToggleRecording");
        if (isRecording()) stopRecording();
        else startRecording();
    }

    @Override
    public void onMessageStopService() {
        L.d("service state onMessageStopService");
        if (isRecording()) stopRecording();
        serviceController.finishListening();
        state = State.SERVICE_WAIT;
        serviceController.stopService();

    }

    private void startRecording() {
        if (isRecording() || !isListening()) {
            throw new IllegalStateException("service state " + state + " received command to start record");
        }
        state = State.SERVICE_RECORDING;
        serviceController.startRecording();
    }

    private void stopRecording() {
        if (!isRecording()) {
            throw new IllegalStateException("service state " + state + " received command to stop record");
        }
        state = State.SERVICE_LISTENING;
        serviceController.stopRecording();
    }
}
