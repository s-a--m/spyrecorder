package com.samart.spyrecorder.app;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.Messenger;

import com.samart.spyrecorder.app.util.L;

import java.io.IOException;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class VolumeButtonListenerService extends Service {
    private ServiceMessenger serviceMessenger;
    private VolumeButtonReceiver broadcastButtonsReceiver;
    private NoSleepManager noSleepMan;
    private MicRecorderMan micRecorderMan;
    private FilesManager filesMan;
    private ServiceStateListener serviceState;
    private ServiceNotificationListener notificationMan;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        L.d("service on start command"); //this.startForeground(R.string.service_waiting_msg, mNotification);
        return Service.START_STICKY;
    }

    @Override
    public void onCreate() {
        L.d("service onCreate");
        super.onCreate();
        this.noSleepMan = new NoSleepManager(this.getApplicationContext());
        this.serviceState = new ServiceState(this.new ServiceControllerImpl());
        this.serviceMessenger = new ServiceMessenger((ServiceState) this.serviceState);
        this.filesMan = new FilesManager();
        this.notificationMan = new NotificationMan(this.getApplicationContext());
        this.micRecorderMan = new MicRecorderMan();
        this.broadcastButtonsReceiver = new VolumeButtonReceiver();
        this.serviceState.onServiceCreate();
    }

    @Override
    public void onDestroy() {
        L.d("service onDestroy");
        this.serviceState.onServiceDestroy();
        this.serviceMessenger.notifyServiceDestroyed();
        super.onDestroy();
    }

    /**
     * returns {@link Messenger} to
     * communicate others with this
     * service
     *
     * @param intent no care
     * @return messenger instance
     */
    @Override
    public IBinder onBind(Intent intent) {
        L.d("service onBind");
        return serviceMessenger.getBinder();
    }

    public interface ServiceController {
        void startRecording();

        void stopRecording();

        void finishListening();

        void startListening();

        void stopService();
    }

    public class ServiceControllerImpl implements ServiceController {

        private boolean isReceiverRegistered;

        @Override
        public void startRecording() {
            L.d("service controller start recording");
            VolumeButtonListenerService.this.micRecorderMan.prepareRecorder(filesMan.getOutputFileName());
            try {
                VolumeButtonListenerService.this.micRecorderMan.startRecording();
                VolumeButtonListenerService.this.notificationMan.onStartRecording(filesMan.getOutputFileName());
                VolumeButtonListenerService.this.serviceMessenger.notifyStateRecording();
            } catch (IOException e) {
                L.e("failed to start recording ", e);
                VolumeButtonListenerService.this.micRecorderMan.abortPreparation();
            }
        }

        @Override
        public void stopRecording() {
            L.d("service controller stop recording");
            VolumeButtonListenerService.this.micRecorderMan.stopRecording();
            VolumeButtonListenerService.this.notificationMan.onStopRecording();
            VolumeButtonListenerService.this.serviceMessenger.notifyStateRecording();
        }

        @Override
        public void finishListening() {
            L.d("service controller finish listening");
            VolumeButtonListenerService.this.notificationMan.onFinishListening();
            if (isReceiverRegistered)
                unregisterReceiver(VolumeButtonListenerService.this.broadcastButtonsReceiver);
            this.isReceiverRegistered = false;
            VolumeButtonListenerService.this.noSleepMan.stopNoSleep();
            VolumeButtonListenerService.this.serviceMessenger.notifyStateListening();
        }

        @Override
        public void startListening() {
            L.d("service controller start listening");
            VolumeButtonListenerService.this.notificationMan.onStartListening();
            VolumeButtonListenerService.this.serviceMessenger.notifyStateListening();
            VolumeButtonListenerService.this.noSleepMan.beginNoSleep();
            VolumeButtonListenerService.this.registerReceiver(broadcastButtonsReceiver, new IntentFilter("android.media.VOLUME_CHANGED_ACTION"));
            this.isReceiverRegistered = true;
        }

        @Override
        public void stopService() {
            L.d("service controller stop service");
            VolumeButtonListenerService.this.notificationMan.onStopService();
            VolumeButtonListenerService.this.serviceMessenger.onStopService();
            VolumeButtonListenerService.this.noSleepMan.stopNoSleep();
            VolumeButtonListenerService.this.stopSelf();
        }
    }

}
