package com.samart.spyrecorder.app.util;

import android.os.Environment;

import java.io.File;
import java.io.IOException;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class FileUtils {
    public static boolean isSDCardMounted() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    public static File createDirectory(File root, String dirName) {
        File dir = new File(root, dirName);
        if (!dir.mkdirs()) {
            L.d("could not create directory in " + root.getAbsolutePath() + " with name " + dirName);
        }
        return dir;
    }

    public static File createFile(File root, String fileName) {
        File file = new File(root, fileName);
        try {
            if (!file.createNewFile()) {
                L.d("could not create new file " + fileName + " in directory " + root.getAbsolutePath());
            }
        } catch (IOException e) {
            L.e("could not create new file " + fileName + " in directory " + root.getAbsolutePath(), e);
        }
        return file;
    }
}
