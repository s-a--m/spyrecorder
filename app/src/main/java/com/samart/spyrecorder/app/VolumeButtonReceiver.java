package com.samart.spyrecorder.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.samart.spyrecorder.app.util.L;

/**
 * Copyright (C) 2014 Dmitry Samoylenko
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * Contact email dmitrysamoylenko@gmail.com
 */
public class VolumeButtonReceiver extends BroadcastReceiver {


    private static final int NUM_PRESSES_ACTIVATE = 4;
    private static final long TIME_OUT_MILLIS = 500;
    private static final long MIN_TIME_TRESHOLD_MILLIS = 100;
    private long lastTimeMillis;
    private int numPresses;
    private Messenger mMessenger;

    private void sendMessageToService(Context context) {
        if (null == this.mMessenger) {
            Intent intentService = new Intent(context, VolumeButtonListenerService.class);
            mMessenger = new Messenger(peekService(context, intentService));
        }
        Message msg = new Message();
        msg.what = ServiceMessenger.MSG_PATTERN_MATCHED;
        try {
            mMessenger.send(msg);
        } catch (RemoteException e) {
            L.e("receiver: could not send message to service ", e);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (null == extras) {
            L.d("no extras in intent");
            return;
        }
        if ("android.media.VOLUME_CHANGED_ACTION".equals(intent.getAction())) {
            Object valueObj = extras.get("android.media.EXTRA_VOLUME_STREAM_VALUE");
            if (null == valueObj) {
                L.d("volume value is null");
                return;
            }
            int value = (Integer) valueObj;
            L.d("broadcast received event volume = " + value);
            Object lastValueObj = extras.get("android.media.EXTRA_PREV_VOLUME_STREAM_VALUE");
            if (null == lastValueObj) {
                lastValueObj = value;
            }
            int lastValue = (Integer) lastValueObj;
            if (value < lastValue || value == 0) {
                if (calcIsTimeTrigger(System.currentTimeMillis())) {
                    sendMessageToService(context);
                }
            }
        }
    }

    private boolean calcIsTimeTrigger(long currentTimeMillis) {
        long deltaT = currentTimeMillis - lastTimeMillis;
        if (deltaT < MIN_TIME_TRESHOLD_MILLIS) return false;

        if (deltaT > TIME_OUT_MILLIS) {
            numPresses = 0;
        }
        numPresses++;
        lastTimeMillis = currentTimeMillis;
        if (numPresses >= NUM_PRESSES_ACTIVATE) {
            numPresses = 0;
            return true;
        }
        return false;
    }

}
