package com.samart.spyrecorder.app;

/**
 * copyright (c) 2014 dmitry samoylenko
 * <p/>
 * this program is free software: you can redistribute it and/or modify
 * it under the terms of the gnu general public license as published by
 * the free software foundation, either version 3 of the license, or
 * (at your option) any later version.
 * <p/>
 * this program is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * gnu general public license for more details.
 * <p/>
 * you should have received a copy of the gnu general public license
 * along with this program.  if not, see <http://www.gnu.org/licenses/>.
 * <p/>
 * contact email dmitrysamoylenko@gmail.com
 */
public interface MessengerListener {
    void onMessageStartListening();

    void onMessageStopListening();

    void onMessageToggleRecording();
    void onMessageStopService();

    boolean isListening();

    boolean isRecording();

    int getState();
}
